The Harris Firm is a law firm that helps individuals throughout Alabama mainly in the areas of bankruptcy, family law, divorce, probate, and injury. Many of these cases are worked on a retainer basis or contingency fee.

Address: 1302 Noble St, #2A, Anniston, AL 36201, USA

Phone: 256-689-1119

Website: https://www.theharrisfirmllc.com/annistondivorce
